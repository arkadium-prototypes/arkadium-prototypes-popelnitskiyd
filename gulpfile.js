var gulp = require('gulp');
var rimraf = require('gulp-rimraf');
var util = require('gulp-util');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var processhtml = require('gulp-processhtml');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var zip = require('gulp-zip');
var queue = require('run-sequence');
var browserfy = require('gulp-browserify');
var bower = require('gulp-bower');

var paths = {
    all: ['src/*.*', 'src/**.*'],
    js: 'src/js/main.js',
    libs: ['bower_components/jquery/dist/jquery.js', 'bower_components/phaser/build/phaser.js'],
    css: 'src/css/*.css',
    html: 'src/*.html',
    assets: ['src/assets/*.*', 'src/assets/**/*.*'],
    dist: 'dist/',
    deploy: 'deploy/'
};

gulp.task('clean', function() {
    return gulp.src(paths.dist, {read: false})
        .pipe(rimraf({forse: false}))
        .on('error', util.log);
});

gulp.task('libs', function() {
    return gulp.src(paths.libs)
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.dist + 'libs/'))
        .on('error', util.log);
});

gulp.task('js:dev', function() {
    return gulp.src(paths.js, {read: false})
        .pipe(browserfy({debug: true}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.dist))
        .on('error', util.log);
});

gulp.task('js:release', function() {
    return gulp.src(paths.js)
        .pipe(browserfy({debug: false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist))
        .on('error', util.log);
});

gulp.task('css', function() {
    return gulp.src(paths.css)
           .pipe(cssmin())
           .pipe(rename({suffix: '.min'}))
           .pipe(gulp.dest(paths.dist + 'css/'))
           .on('error', util.log);
});

gulp.task('html', function() {
    return gulp.src(paths.html)
               .pipe(processhtml())
               .pipe(gulp.dest(paths.dist))
               .on('error', util.log);
});

gulp.task('assets', function() {
    return gulp.src(paths.assets)
               .pipe(gulp.dest(paths.dist + 'assets/'))
               .on('error', util.log);
});

gulp.task('connect', function() {
    connect.server({root: [paths.dist], port: 9000, livereload: false});
});

gulp.task('reconnect', function() {
    gulp.src(paths.dist).pipe(connect.reload()).on('error', util.log);
});

gulp.task('watch', function() {
    return gulp.watch(paths.all, ['recompile']);
});

gulp.task('bower:install', function() {
    return bower();
});

gulp.task('package', function() {
    return gulp.src([paths.dist + '*', paths.dist + '**/*'])
           .pipe(zip('archive.zip'))
           .pipe(gulp.dest(paths.deploy))
           .on('error', util.log);
});

gulp.task('compile', function() {
    queue('clean', 'libs', 'js:dev', 'css', 'html', 'assets', 'connect', 'watch');
});

gulp.task('recompile', function() {
    queue('js:dev', 'css', 'html', 'assets', 'reconnect');
});

gulp.task('default', ['compile']);