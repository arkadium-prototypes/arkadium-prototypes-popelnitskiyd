var GameSettings = require('../GameSettings');

var CellView = function(game, cell, controller) {
    "use strict";
    Phaser.Group.call(this, game);

    this.game = game;
    this.cell = cell;
    this.controller = controller;
    this.elementView = null;
    this.game.input.onUp.add(this.onMouseUpEvent, this);
    //this.game.input.onDown.add(this.onMouseDownEvent, this);
};

module.exports = CellView;

CellView.prototype = Object.create(Phaser.Group.prototype);
CellView.prototype.init = function() {
    "use strict";
    this.createCell();
};
CellView.prototype.createCell = function() {
    "use strict";
    var border = new Phaser.Sprite(this.game, 0, 0, GameSettings.gameAtlas, "border.png");
    var cell = new Phaser.Sprite(this.game, 0, 0, GameSettings.gameAtlas, "cell.png");
    
    this.addChild(cell);
    this.addChild(border);
};
CellView.prototype.onMouseUpEvent = function() {
    "use strict";
    var movedElement = this.controller.getMovingElement();

    if(movedElement === null) {
        return;
    }

    var movedElementBox = movedElement.getBounds();
    var thisBox = this.getBounds();

    if(Phaser.Rectangle.contains(movedElementBox, thisBox.centerX, thisBox.centerY)) {
        if (this.elementView === null && this.cell.type == 0) {
            var cellViewList = this.controller.getAllCellViews();

            for(var i = 0; i < cellViewList.length; i++) {
                var cell = cellViewList[i].cell;

                if(cell.row == movedElement.model.row && cell.column == movedElement.model.column) {
                    cell.type = 0;
                    cellViewList[i].elementView = null;
                    movedElement.model.row = this.cell.row;
                    movedElement.model.column = this.cell.column;
                }
            }

            this.elementView = movedElement;
            movedElement.x = this.x + this.width * 0.5;
            movedElement.y = this.y + this.height * 0.5;
        } else {
            movedElement.x = movedElement.previosPos.x;
            movedElement.y = movedElement.previosPos.y;
        }

        //this.controller.moveElement(null);
    }
};
CellView.prototype.constructor = CellView;