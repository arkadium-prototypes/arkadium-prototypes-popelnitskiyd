var CellView = require('./CellView');
var ElementView = require('./ElementView');
var ElementModel = require('../Model/ElementModel');
var GridController = require("../Controller/GridController");
var GameSettings = require('../GameSettings');

var GridView = function(game, grid) {
    "use strict";
    Phaser.Group.call(this, game);
    
    this.game = game;
    this.grid = grid;
    this.cellViewList = [];
    this.elementViewList = [];
    this.movingElement = null;
    this.arrowView = null;
    this.controller = null;
    this.stars = 0;
    this.onFinished = new Phaser.Signal();
};

module.exports = GridView;

GridView.prototype = Object.create(Phaser.Group.prototype);
GridView.prototype.init = function() {
    "use strict";
    this.createController();
    this.createGrid();
    this.createElements();
};
GridView.prototype.createController = function() {
    "use strict";
    this.controller = new GridController(this);
};
GridView.prototype.createGrid = function() {
    "use strict";
    var cells = this.grid.cells;

    for(var i = 0; i < cells.length; i++) {
        var cell = cells[i];
        var cellView = new CellView(this.game, cell, this.controller);

        cellView.init();
        cellView.x = (cell.column - 1) * cellView.width;
        cellView.y = (cell.row - 1) * cellView.height;

        this.addChild(cellView);
        this.cellViewList.push(cellView);
    }
};
GridView.prototype.createElements = function() {
    "use strict";
    var scale = Math.min(this.game.width / 800, this.game.height / 600);
    var half = 0.5;
    var element = null;
    var cell = null;

    for(var i = 0; i < this.cellViewList.length; i++) {
        var cellView = this.cellViewList[i];
        cell = cellView.cell;

        if(cell.type !== GameSettings.Element.Empty) {
            element = this.controller.getElementModelByType(cell.type);
            element.row = cell.row;
            element.column = cell.column;

            var elementView = new ElementView(this.game, new ElementModel(element), this.controller);

            elementView.init();
            elementView.scale.setTo(cellView.scale.x * 0.9, cellView.scale.y * 0.9);
            elementView.anchor.setTo(half, half);
            elementView.pivot.setTo(half, half);
            elementView.x = cellView.x + cellView.width * half;
            elementView.y = cellView.y + cellView.height * half;
            cellView.elementView = elementView;

            this.addChild(elementView);


            if(cell.type === GameSettings.Element.LoadedBow) {
                element = this.controller.getElementModelByType(GameSettings.Element.Arrow);
                element.row = cell.row;
                element.column = cell.column;
                this.arrowView = new ElementView(this.game, new ElementModel(element), this.controller);
                this.arrowView.init();
                this.arrowView.scale.setTo(cellView.scale.x * 0.9, cellView.scale.y * 0.9);
                this.arrowView.anchor.setTo(half, half);
                this.arrowView.x = cellView.x + cellView.width * half;
                this.arrowView.y = cellView.y + cellView.height * half;
                this.addChildAt(this.arrowView, 0);
            } else {
                this.elementViewList.push(elementView);
            }
        }
    }
};
GridView.prototype.constructor = GridView;