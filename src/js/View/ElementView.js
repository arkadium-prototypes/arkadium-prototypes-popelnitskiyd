/**
 * Created by popelnitskiyd on 16.10.2015.
 */
var GameSettings = require('../GameSettings');

var ElementView = function(game, model, controller) {
    "use strict";
    this.game = game;
    this.model = model;
    this.controller = controller;
    this.rotationOffset = 1;
    this.isRotated = false;
    this.isCollided = false;
    this.isMoved = false;
    this.isMoving = false;
    this.isShot = false;
    this.previosPos = new Phaser.Point(0, 0);
    this.scale = 0;
    this.game.input.onUp.add(this.onMouseUpEvent, this);
    this.game.input.onDown.add(this.onMouseDownEvent, this);
};

module.exports = ElementView;

ElementView.prototype = Object.create(Phaser.Sprite.prototype);
ElementView.prototype.init = function() {
    "use strict";
    this.scale = Math.min(this.game.width / 800, this.game.height / 600);
    var frameKey = "";

    switch (this.model.type) {
        case GameSettings.Element.Apple: {
            frameKey = "apple.png";
        } break;
        case GameSettings.Element.Arrow: {
            frameKey = "arrow.png";
        } break;
        case GameSettings.Element.Star: {
            frameKey = "star.png";
        } break;
        case GameSettings.Element.LoadedBow: {
            frameKey = "bow_shot.png";
            this.isRotated = true;
            this.rotationOffset = -0.6;
        } break;
        case GameSettings.Element.M0:
        case GameSettings.Element.M90:
        case GameSettings.Element.M180:
        case GameSettings.Element.M270: {
            frameKey = "mirror.png";
            this.isMoved = true;
            this.isRotated = true;
        } break;
        default : {
            frameKey = "cell.png";
        } break;
    }

    Phaser.Sprite.call(this, this.game, 0, 0, GameSettings.gameAtlas, frameKey);

    this.angle = this.model.a * this.rotationOffset;
    this.game.physics.arcade.enable(this);
    this.body.allowGravity = false;
};
ElementView.prototype.onMouseUpEvent = function() {
    "use strict";
    this.isMoving = false;

    var activePointer = this.game.input.activePointer;
    var movedElement = this.controller.getMovingElement();
    var clickOffset = activePointer.timeUp - activePointer.timeDown;

    if(this === movedElement) {
        this.controller.moveElement(null);

        if(this.isRotated && clickOffset < 100) {
            this.angle += 90;

            switch(this.angle) {
                case 0: {
                    this.model = this.controller.getElementModelByType(GameSettings.Element.M0);
                } break;
                case 90: {
                    this.model = this.controller.getElementModelByType(GameSettings.Element.M90);
                } break;
                case -180: {
                    this.model = this.controller.getElementModelByType(GameSettings.Element.M180);
                } break;
                case -90: {
                    this.model = this.controller.getElementModelByType(GameSettings.Element.M270);
                } break;
                default : {} break;
            }
        }
    }
 };
ElementView.prototype.onMouseDownEvent = function() {
    "use strict";
    var activePos = this.game.input.activePointer.position;

    if(Phaser.Rectangle.contains(this.getBounds(), activePos.x, activePos.y)) {
        this.previosPos.setTo(this.x, this.y);
        this.parent.addChildAt(this, this.parent.children.length - 1);
        this.isMoving = true;
    }

    if(this.isMoved && this.isMoving) {
        this.controller.moveElement(this);
    }
};
ElementView.prototype.update = function() {
    "use strict";
    if(this.isMoved && this.isMoving) {
        var activePos = this.game.input.activePointer.position;

        this.x = (activePos.x - this.parent.x) / this.parent.scale.x;
        this.y = (activePos.y - this.parent.y) / this.parent.scale.y;
    }

    if(this.isShot) {
        var vectorX = this.model.vectorX;
        var vectorY = this.model.vectorY;

        if(vectorX.x != 0) this.x += 5 * vectorX.x;

        if(vectorY.y != 0) this.y += 5 * vectorY.y;

        var firstCell = this.parent.cellViewList[0];
        var lastCell = this.parent.cellViewList[this.parent.cellViewList.length - 1];
        var parentEndX = (lastCell.x + lastCell.width) * this.parent.scale.x;
        var parentEndY = 6 * firstCell.height * this.parent.scale.y;

        if((this.x - this.width * 0.5) <= 0 || (this.y - this.width * 0.5) <= 0
            || ((this.x + this.width * 0.5) * this.parent.scale.x) >= parentEndX
            || ((this.y + this.width * 0.5) * this.parent.scale.y) >= parentEndY) {
            this.isShot = false;
            this.parent.onFinished.dispatch({success: false, stars: this.parent.stars});
            this.kill();
        }
    }
};
ElementView.prototype.constructor = ElementView;