// ========================= Construction =========================
var GameSettings = {
    // Variables (manually set)
    version: "0.01",
    DEBUG: false,

    // Atlases
    assetScale: 1,
    gameAtlas: "gameAtlas",
    menuAtlas: "menuAtlas",

    // Fonts
    mainFont: "Gotham-Medium-50",
    subFont: "Gotham-Book-50",

    // App variables
    browserName: "",
    currentDevice: 0,
    currentPlatform: 0,
    isWebKitBrowser: false,
    isChromeBrowser: false,
    screenWidth: 0,
    screenHeight: 0,

    // Game variables (automatically set)
    allowInterstitialAds: false,
    allowPurchasing: false,
    hasRetinaDisplay: false,
    isWebGLEnabled: true,
    isLocalStorageEnabled: true,
    showPreloadAnimation: true
};
module.exports = GameSettings;

// ========================= Enums =========================
GameSettings.AssetScales = {
    small: 0.5,
    medium: 0.75,
    large: 1
};
Object.freeze(GameSettings.AssetScales);

GameSettings.Devices = {
    Desktop: 0,
    Tablet: 1,
    Phone: 2
};
Object.freeze(GameSettings.Devices);

GameSettings.InputPriorities = {
    Background: 0,
    Objects: 1,
    Scrim: 2,
    Popups: 3
};
Object.freeze(GameSettings.InputPriorities);

GameSettings.Platforms = {
    Arena: 0,
    Android: 1,
    iOS: 2
};
Object.freeze(GameSettings.Platforms);

GameSettings.PopupStates = {
    hidden: 0, // Popup is not shown
    appearing: 1, // Popup is playing a tween-in animation
    visible: 2, // Popup is shown onscreen
    disappearing: 3 // Popup is playing a tween-out animation, returns to Hidden state afterwards
};
Object.freeze(GameSettings.PopupStates);

GameSettings.Side = {
    None: 0,
    Left: 1,
    Top: 2,
    Right: 3,
    Bottom: 4
};
Object.freeze(GameSettings.Side);

GameSettings.Element = {
    Empty: 0,
    M0: 1,
    M90: 2,
    M180: 3,
    M270: 4,
    Star: 5,
    LoadedBow: 6,
    Apple: 7,
    Arrow: 8
};
Object.freeze(GameSettings.Element);