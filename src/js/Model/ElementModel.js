var ElementModel = function(data) {
    "use strict";
    this.type = data.type;
    this.a = data.a;
    this.x = data.x;
    this.y = data.y;
    this.wx = data.wx;
    this.wy = data.wy;
    this.vectorX = {
        a: data.vectorX.a,
        x: data.vectorX.x,
        y: data.vectorX.y,
        d: data.vectorX.d
    };
    this.vectorY = {
        a: data.vectorY.a,
        x: data.vectorY.x,
        y: data.vectorY.y,
        d: data.vectorY.d
    };
    this.row = data.row;
    this.column = data.column;
};

module.exports = ElementModel;

ElementModel.prototype.constructor = ElementModel;