var CellModel = require('./CellModel');

var GridModel = function(level, elements) {
    "use strict";
    this.rowCount = level.rows;
    this.columnCount = level.columns;
    this.field = level.field;
    this.cells = [];
    this.elements = elements;
    this.init();
};

module.exports = GridModel;

GridModel.prototype = {
    init: function() {
        "use strict";
        for(var i = 1; i <= this.field.length; i++) {
            var row = this.field[i - 1];

            for(var j = 1; j <= row.length; j++) {
                this.cells.push(new CellModel(row[j - 1], i, j));
            }
        }
    }
};
GridModel.prototype.constructor = GridModel;