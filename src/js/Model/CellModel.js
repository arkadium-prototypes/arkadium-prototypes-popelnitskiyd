var CellModel = function(type, row, column) {
    "use strict";
    this.type = type;
    this.row = row;
    this.column = column;
};

module.exports = CellModel;

CellModel.prototype.constructor = CellModel;