'use strict';
// Create the Phaser game
var gameWidth = (window.innerWidth > 0) ? window.innerWidth : 640;
var gameHeight = (window.innerHeight > 0) ? window.innerHeight : 480;
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'prototype-game'); // jshint ignore:line

game.state.add('Boot', require('./states/Boot'));
game.state.add('Preloader', require('./states/Preloader'));
game.state.add('Menu', require('./states/Menu'));
game.state.add('Game', require('./states/Game'));

game.state.start('Boot');