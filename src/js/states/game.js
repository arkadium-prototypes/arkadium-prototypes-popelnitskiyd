/**
 * Created by popelnitskiyd on 15.10.2015.
 */

var GameSettings = require('../GameSettings');
var GridView = require('../View/GridView');
var GridModel = require('../Model/GridModel');
var CellView = require('../View/CellView');
var CellModel = require('../Model/CellModel');

var Game = function(game) {
    "use strict";
    this.info = null;
    this.gridView = null;
    this.arrow = null;
    this.elements = null;
    this.stars = 0;
    this.starsBar = [];
    this.nextLevel = 0;
};

module.exports = Game;

Game.prototype = {
    init: function(data) {
        "use strict";
        if(Number.isFinite(data)) {
            this.nextLevel = data;
        }
    },
    preload: function() {
        "use strict";
    },

    create: function() {
        "use strict";
        this.info = this.game.cache.getJSON('levels');
        this.createUI();
        this.arrow = this.gridView.arrowView;
        this.elements = this.gridView.elementViewList;
    },

    update: function() {
        "use strict";
        this.game.physics.arcade.overlap(this.arrow, this.elements, this.onCollided, null, this);
    },

    destroy: function() {
        "use strict";
        this.arrow = null;
        this.elements = null;
        this.gridView.elementViewList.length = 0;
        this.gridView.cellViewList.length = 0;
        this.game.state.clearCurrentState();
    },
    
    createUI: function() {
        "use strict";
        var scale = Math.min(this.game.width / 800, this.game.height / 600);
        var background = new Phaser.Sprite(this.game, 0, 0, GameSettings.gameAtlas, "background.jpg");
        
        this.game.add.existing(background);
        background.width = this.game.width;
        background.height = this.game.height;

        var level = this.info.data.levels[this.nextLevel];
        var elements = this.info.data.elements;
        var model = new GridModel(level, elements);
        this.gridView = new GridView(this.game, model);

        this.gridView.init();
        this.game.add.existing(this.gridView);
        this.gridView.scale.x = this.gridView.scale.y = scale * 0.7;
        this.gridView.x = (this.game.width - this.gridView.width) * 0.5;
        this.gridView.y = 50 * scale;
        this.gridView.onFinished.add(this.endLevel, this);

        this.createUIControls(scale);
    },
    createUIControls: function(scale) {
        "use strict";
        this.runButton = new Phaser.Button(this.game, 0, 0, GameSettings.gameAtlas, this.shoot, this, "run-normal.png", "run-normal.png", "run-clicked.png", "run-normal.png");

        this.game.add.existing(this.runButton);
        this.runButton.scale.x = this.runButton.scale.y = scale * 0.8;
        this.runButton.anchor.setTo(0.5, 0);
        this.runButton.x = this.game.width * 0.5;
        this.runButton.y = (this.gridView.y + this.gridView.height) + 20 * scale;
        this.titleLabel = new Phaser.Text(this.game, 0, 0, "", {
            'fontSize': '30px',
            'fill': "rgba(255,255,255, 1)"});

        this.game.add.existing(this.titleLabel);
        this.titleLabel.visible = false;
        this.titleLabel.anchor.setTo(0.5, 0.5);
        this.titleLabel.scale.setTo(scale, scale);
        this.titleLabel.x = this.game.width * 0.5;
        this.titleLabel.y = 20 * scale;
        var levelLabel = new Phaser.Text(this.game, 0, 0, "Level " + (this.nextLevel + 1), {
            'fontSize': '30px',
            'fill': "rgba(255,255,255, 1)"});

        this.game.add.existing(levelLabel);
        levelLabel.anchor.setTo(1, 0.5);
        levelLabel.scale.setTo(scale, scale);
        levelLabel.x = this.game.width - 20 * scale;
        levelLabel.y = 20 * scale;

        this.starsBarView = new Phaser.Group(this.game);

        for(var i = 0; i < 3; i++) {
            var back = new CellView(this.game, new CellModel(0, 1, i + 1), this.gridView.controller);
            var star = new Phaser.Sprite(this.game, 0, 0, GameSettings.gameAtlas, "star.png");

            star.visible = false;
            back.init();
            back.addChild(star);
            back.x = (back.cell.column - 1) * back.width;

            this.starsBarView.addChild(back);
        }

        this.game.add.existing(this.starsBarView);

        this.starsBarView.scale.x = this.starsBarView.scale.y = this.gridView.scale.x * 0.5;
        this.starsBarView.x = 20 * scale;
        this.starsBarView.y = 10 * scale;

    },

    endLevel: function(data) {
        "use strict";
        var message = "";
        if(data.success) {
            message = "You WIN!!!";
        } else {
            message = "You lose";
        }

        this.titleLabel.setText(message);
        this.titleLabel.visible = true;

        setTimeout((function() {
            if(data.success && this.nextLevel + 1 < this.info.data.levels.length) {
                this.game.state.restart(true, false, this.nextLevel + 1);
            } else {
                this.game.state.start('Menu');
            }

            this.destroy();
        }).bind(this), 5000);
    },

    shoot: function() {
        "use strict";
        var elements = this.gridView.elementViewList;

        for(var i = 0; i < elements.length; i++) {
            elements[i].isMoved = false;
        }

        var arrow = this.gridView.arrowView;

        arrow.isShot = true;
        arrow.parent.addChildAt(arrow, arrow.parent.children.length - 1);
        this.runButton.kill();
    },

    onCollided: function(arrow, element) {
        "use strict";
        var arrowBox = arrow.getBounds();
        var elementBox = element.getBounds();

        if(arrow.isShot && !element.isCollided && Phaser.Rectangle.containsRect(arrowBox, elementBox)) {
            switch (element.model.type) {
                case GameSettings.Element.Apple: {
                    element.kill();
                    arrow.isShot = false;
                    arrow.kill();
                    this.gridView.onFinished.dispatch({success: true, stars: this.gridView.stars});
                } break;
                case GameSettings.Element.Star: {
                    element.kill();
                    this.gridView.stars += 1;

                    for(var i = 0; i < this.gridView.stars; i++) {
                        var child = this.starsBarView.children[i];

                        for(var j = 0; j < child.children.length; j++) {
                            child.children[j].visible = true;
                        }
                    }
                } break;
                case GameSettings.Element.M0:
                case GameSettings.Element.M90:
                case GameSettings.Element.M180:
                case GameSettings.Element.M270: {
                    if(element.model.wx == arrow.model.x || element.model.wy == arrow.model.y) {
                        arrow.isShot = false;
                        arrow.kill();
                        this.gridView.onFinished.dispatch({success: false, stars: this.gridView.stars});
                    }

                    if (element.model.x == arrow.model.x) {
                        arrow.angle = element.model.vectorY.a;
                        arrow.model.vectorX = arrow.model.vectorY = element.model.vectorY;
                        arrow.model.x = GameSettings.Side.None;
                        arrow.model.y = element.model.vectorY.d;
                        arrow.x = element.x;
                    } else if (arrow.model.y == element.model.y) {
                        arrow.angle = element.model.vectorX.a;
                        arrow.model.vectorX = arrow.model.vectorY = element.model.vectorX;
                        arrow.model.x = element.model.vectorX.d;
                        arrow.model.y = GameSettings.Side.None;
                        arrow.y = element.y;
                    }
                } break;
                default: {} break;
            }

            element.isCollided = true;
        }
    }
};

Game.prototype.constructor = Game;