/**
 * Created by popelnitskiyd on 15.10.2015.
 */
var GameSettings = require("../Gamesettings");

var Preloader = function(game) {
    "use strict";
};

module.exports = Preloader;

Preloader.prototype = {
    preload: function() {
        "use strict";
        this.determineDevice();
        this.loadContent();
    },

    create: function() {
        "use strict";
        setTimeout((function() {
            $('#preloader').css('visibility', 'hidden');
            this.game.state.start('Menu');
        }).bind(this), 2000);
    },

    determineDevice: function () {
        "use strict";

        if(this.game.device.desktop || (this.game.device.ie && !this.game.device.windowsPhone)) {
            GameSettings.currentDevice = GameSettings.Devices.Desktop;
        } else if(this.game.device.iPad || Math.min(window.innerWidth, window.innerHeight) > 800) {
            GameSettings.currentDevice = GameSettings.Devices.Tablet;
        } else {
            GameSettings.currentDevice = GameSettings.Devices.Phone;
        }
    },

    loadContent: function() {
        "use strict";
        this.game.load.atlas(GameSettings.gameAtlas, "assets/images/" + GameSettings.gameAtlas + ".png",
                                                     "assets/images/" + GameSettings.gameAtlas + ".json");
        this.game.load.json("levels", "assets/data/levels.json");
    }
};

Preloader.prototype.constructor = Preloader;