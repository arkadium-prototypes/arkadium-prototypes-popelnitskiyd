/**
 * Created by popelnitskiyd on 15.10.2015.
 */

var Boot = function(game) {
    "use strict";
};

module.exports = Boot;

Boot.prototype = {
    preload: function() {
        "use strict";
        this.game.stage.setBackgroundColor('0xffffff');
    },

    create: function() {
        "use strict";
        this.game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;

        this.game.state.start('Preloader');
    }
};

Boot.prototype.constructor = Boot;