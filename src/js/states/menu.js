/**
 * Created by popelnitskiyd on 15.10.2015.
 */
var GameSettings = require("../GameSettings");

var Menu = function(game) {
    "use strict";
};

module.exports = Menu;

Menu.prototype = {
    preload: function() {
        "use strict";
    },

    create: function() {
        "use strict";
        var scale = Math.min(this.game.width / 800, this.game.height / 600);
        var background = new Phaser.Sprite(this.game, 0, 0, GameSettings.gameAtlas, "background.jpg");

        background.width = this.game.width;
        background.height = this.game.height;
        this.game.add.existing(background);

        var data = this.game.cache.getJSON('levels');
        var title = new Phaser.Text(this.game, 0, 0, data.name, {
            'fontSize': '50px',
            'fill': "rgba(10,10,10, 1)"});

        this.game.add.existing(title);
        title.anchor.setTo(0.5, 0.5);
        title.scale.setTo(scale, scale);
        title.x = this.game.width * 0.5;
        title.y = 200 * scale;
        var play = new Phaser.Button(this.game, 0, 0, GameSettings.gameAtlas, this.startGame, null, "play.png", "play.png", "play.png", "play.png");

        this.game.add.existing(play);
        play.anchor.setTo(0.5, 0.5);
        play.scale.setTo(scale, scale);
        play.x = this.game.width * 0.5;
        play.y = this.game.height - 100 * scale;
    },

    startGame: function() {
        "use strict";
        this.game.state.start("Game", true, false, 0);
    }
};

Menu.prototype.constructor = Menu;