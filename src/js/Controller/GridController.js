var GridController = function(gridView) {
    "use strict";
    this.gridView = gridView;
};

module.exports = GridController;

GridController.prototype = {
    moveElement: function(element) {
        "use strict";
        this.gridView.movingElement = element;
    },
    getMovingElement: function() {
        "use strict";
        return this.gridView.movingElement;
    },
    getAllCellViews: function() {
        "use strict";
        return this.gridView.cellViewList;
    },
    getElementModelByType: function(type) {
        "use strict";
        var elements = this.gridView.grid.elements;

        for(var i = 0; i < elements.length; i++) {
            var element = elements[i];

            if(element.type === type) {
                return element;
            }
        }

        return null;
    }
};

GridController.prototype.constructor = GridController;